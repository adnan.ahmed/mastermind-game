// default
// Create a color-code Map
const colors = new Map([
    ["yellow", "#FFB400"],
    ["persimmon", "#FF5A5F"],
    ["pastel-green", "#8CE071"],
    ["robins-egg-blue", "#00D1C1"],
    ["blue-lagoon", "#007A87"],
    ["siren", "#7B0051"]
]);

const htmlElementRefs = {
    showRuleBtn: null,
    rules: null,
    endgameModal: null,
    endgameResult: null,
    decodeRows: null,
    submitBtn: null,
    colorPicker: null,
}

const ARRAY_LENGTH = 4;
let masterMindCode = [];
let guessMap = new Map();
let guessResult = [];
let selectedColor = "";
let activeRow = 1;


// get refrences
window.onload = init();


function init() {
    // Get elements references
    htmlElementRefs.showRuleBtn = document.getElementById("show-rules-btn");
    htmlElementRefs.rules = document.getElementById("rules");
    htmlElementRefs.endgameModal = document.getElementById("endgame");
    htmlElementRefs.colorPicker = document.getElementById("color-picker");
    htmlElementRefs.endgameResult = document.getElementById("endgame-result");
    htmlElementRefs.decodeRows = document.querySelectorAll('.decode-row div');
    htmlElementRefs.submitBtn = document.querySelectorAll(".submit-button");

    // Add event listener
    htmlElementRefs.showRuleBtn.addEventListener('click', toggleRules);
    htmlElementRefs.showRuleBtn.addEventListener('click', toggleRules);
    htmlElementRefs.colorPicker.addEventListener('change', selectColor);

    document.addEventListener('DOMContentLoaded', function () {
        let colorArray = Array.from(colors.keys());
        for (let count = 0; count < ARRAY_LENGTH; count++) {
            masterMindCode.push(colors.get(colorArray[Math.floor(Math.random() * 6)]))
        }
        selectColor();
    });

    for (const element of htmlElementRefs.decodeRows) {
        element.addEventListener('click', function () {
            fillColor(this.id);
        });
    }

    for (const element of htmlElementRefs.submitBtn) {
        element.addEventListener('click', submitGuess);
    }
}

function resetGame() {
    location.reload();
}

function toggleRules() {
    htmlElementRefs.rules.classList.toggle("show-rules");
}

function selectColor() {
    selectedColor = colors.get(document.querySelector('input[name = color]:checked').value);
}

function changeSubmitButtonState() {
    let submitBtn = document.querySelector(`#row-${activeRow} #submit-btn`);
    if (submitBtn.className === "submit hidden") {
        submitBtn.className = "submit";
    }
    else {
        submitBtn.className = "submit hidden";
    }
}

function changeRow() {
    let currentRow = document.getElementById(`row-${activeRow}`);
    currentRow.className = "row";
    activeRow++;
    currentRow = document.getElementById(`row-${activeRow}`);
    currentRow.className = "row active-row";
}

function fillColor(id) {
    const element = document.getElementById(id);
    element.style.background = selectedColor;
    guessMap.set(element.getAttribute("value"), selectedColor);
    if (guessMap.size === 4) {
        changeSubmitButtonState();
    }
}

function showResult(result) {
    if (result === true) {
        htmlElementRefs.endgameModal.className += " success";
        htmlElementRefs.endgameResult.innerHTML = "CONGRATULATIONS!";
    }
    else {
        htmlElementRefs.endgameModal.className += " failure";
        htmlElementRefs.endgameResult.innerHTML = "GAME OVER!";
    }
}

function shuffleGuessResult() {
    let randomIndex, temp;
    for (let index = 0; index < ARRAY_LENGTH; index++) {
        randomIndex = Math.floor(Math.random() * 4);
        temp = guessResult[index];
        guessResult[index] = guessResult[randomIndex];
        guessResult[randomIndex] = temp;
    }
}

function displayGuessResult() {
    for (let index = 0; index < ARRAY_LENGTH; index++) {
        if (guessResult[index] === "x") {
            document.querySelector('#row-' + activeRow + ' .hint-row :nth-child(' + (index + 1).toString() + ')').className = "hint none-match";
        }
        else if (guessResult[index] === "o") {
            document.querySelector('#row-' + activeRow + ' .hint-row :nth-child(' + (index + 1).toString() + ')').className = "hint exact-match";
        }
    }
}

function submitGuess() {
    guessResult.length = 0;
    let matched = true;
    for (let i = 0; i < ARRAY_LENGTH; i++) {
        if (guessMap.get(i.toString()) !== masterMindCode[i]) {
            matched = false;
            if (masterMindCode.includes(guessMap.get(i.toString()))) {
                let index = masterMindCode.indexOf(guessMap.get(i.toString()));
                if (guessMap.get(index.toString()) != masterMindCode[index]) {
                    guessResult[i] = "-";
                }
                else {
                    guessResult[i] = "x";
                }
            }
            else {
                guessResult[i] = "x";
            }
        }
        else {
            guessResult[i] = "o";
        }
    }
    shuffleGuessResult();
    displayGuessResult();
    if (matched == false) {
        guessMap.clear();
        changeSubmitButtonState();
        if (activeRow < 10) {
            changeRow();
        }
        else {
            showResult(false);
        }
    }
    else {
        showResult(true);
    }
}